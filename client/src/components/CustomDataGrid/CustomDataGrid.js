import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import './CustomDataGrid.css';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const emailTemplates = {
  followup: 'FollowupTemplate.html',
  statement: 'StatementTemplate.html',
};

const CustomDataGrid = () => {
  const [rows, setRows] = useState([]); // data rows
  const [selectedRows, setSelectedRows] = useState([]); // selected rows
  const [error, setError] = useState(''); // error message
  const [loading, setLoading] = useState(true); // loading indicator
  const [page, setPage] = useState(0); // current page number
  const [sortConfig, setSortConfig] = useState({ key: '', direction: 'asc' }); // sorting configuration
  const [refresh, setRefresh] = useState(false); // refresh indicator
  const [searchQuery, setSearchQuery] = useState(''); // search query state
  const pageSize = 1000; // page size

  // const fetchStatements = async () => {
  //   try {
  //     const response = await axios.get(
  //       `${process.env.REACT_APP_API_URL}/statement/list_statements`,
  //       {
  //         headers: {
  //           'X-Dispatch-Key':
  //             'cHVibGljbW91c2VyZWFkdHVybndvcmRzdHJhbmdlcmNvYWNocmVjb3JkdHJvcGljYWxicmFzc3N0b25lYXNsZWVwb3RoZXJ3b3JlZXhj',
  //         },
  //       }
  //     );
  //     setRows(response.data.statements);
  //   } catch (err) {
  //     console.error('Failed to fetch statements:', err);
  //     setError('Failed to fetch statements.');
  //   } finally {
  //     setLoading(false);
  //   }
  // };

  const fetchCustomers = async () => {
    setLoading(true);
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_API_URL}/fetch_customers`,
        {
          customer_number: '',
          customer_name: '',
          running_balance: '',
          sequence: '',
          email_address: '',
        }
      );
      const customers = response.data.filter(
        (customer) => parseFloat(customer.running_balance) > 0
      );
      setRows(customers);
    } catch (err) {
      console.error('Failed to fetch customers:', err);
      setError('Failed to fetch customers.');
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchCustomers();
  }, [refresh]); // Add refresh as a dependency

  const renderBooleanCell = (value) => {
    return value ? (
      <span style={{ color: 'green' }}>✓</span>
    ) : (
      <span style={{ color: 'red' }}>✗</span>
    );
  };

  const handleCheckboxChange = (event, row) => {
    setSelectedRows((prevSelected) =>
      event.target.checked
        ? [...prevSelected, row]
        : prevSelected.filter(
            (selectedRow) => selectedRow.customer_number !== row.customer_number
          )
    );
  };
  const handleSelectAllChange = (event) => {
    if (event.target.checked) {
      setSelectedRows(currentRows);
    } else {
      setSelectedRows([]);
    }
  };

  const sendEmail = (templateName) => {
    const templateFile = emailTemplates[templateName];

    if (!templateFile) {
      console.error(`Template not found for name: ${templateName}`);
      return;
    }

    const emailPromises = selectedRows.map((row) =>
      axios.post(`${process.env.REACT_APP_API_URL}/send_email`, {
        client_number: row.client_number,
        client_name: row.client_name,
        to: row.email,
        ready: row.ready,
        sent: row.sent,
        opened: row.opened,
        follow_up: row.follow_up,
        template: templateFile,
      })
    );

    Promise.all(emailPromises)
      .then(() => {
        console.log('Emails sent successfully.');
        setRefresh((prev) => !prev); // refresh the table
      })
      .catch((error) => {
        console.error('Failed to send emails:', error);
      });

    alert('Emails sent to selected rows.');
  };

  const sortRows = (rows, config) => {
    if (!config.key) return rows;

    return [...rows].sort((a, b) => {
      if (a[config.key] < b[config.key]) {
        return config.direction === 'asc' ? -1 : 1;
      }
      if (a[config.key] > b[config.key]) {
        return config.direction === 'asc' ? 1 : -1;
      }
      return 0;
    });
  };

  const handleSort = (key) => {
    let direction = 'asc';
    if (sortConfig.key === key && sortConfig.direction === 'asc') {
      direction = 'desc';
    }
    setSortConfig({ key, direction });
  };

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filterRows = (rows, query) => {
    return rows.filter((row) => {
      return (
        row.client_number.toString().includes(query) ||
        row.client_name.toLowerCase().includes(query.toLowerCase()) ||
        row.email.toLowerCase().includes(query.toLowerCase())
      );
    });
  };

  const filteredRows = filterRows(rows, searchQuery);
  const sortedRows = sortRows(filteredRows, sortConfig);
  const currentRows = sortedRows.slice(page * pageSize, (page + 1) * pageSize);
  const allSelected =
    currentRows.length > 0 && selectedRows.length === currentRows.length;

  const renderSortArrow = (key) => {
    if (sortConfig.key === key) {
      return sortConfig.direction === 'asc' ? ' ▲' : ' ▼';
    }
    return '';
  };

  return (
    <div style={{ padding: '20px' }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TextField
          label='Search'
          placeholder='Search by client number, client name, or email'
          value={searchQuery}
          onChange={handleSearchChange}
          variant='outlined'
          style={{ marginBottom: '20px', width: '50%' }}
        />
        <Typography variant='h6' component='div'>
          Nombre de clients : {filteredRows.length}
        </Typography>
      </div>
      {loading && <CircularProgress />}
      {error && <div style={{ color: 'red' }}>{error}</div>}
      {!loading && !error && (
        <>
          <table style={{ width: '100%', borderCollapse: 'collapse' }}>
            <thead>
              <tr>
                <th>
                  <input
                    type='checkbox'
                    checked={allSelected}
                    onChange={handleSelectAllChange}
                  />
                </th>
                <th
                  onClick={() => handleSort('id')}
                  style={{ cursor: 'pointer' }}>
                  ID {renderSortArrow('id')}
                </th>
                <th
                  onClick={() => handleSort('client_number')}
                  style={{ cursor: 'pointer' }}>
                  Client Number {renderSortArrow('client_number')}
                </th>
                <th
                  onClick={() => handleSort('client_name')}
                  style={{ cursor: 'pointer' }}>
                  Client Name {renderSortArrow('client_name')}
                </th>
                <th
                  onClick={() => handleSort('email')}
                  style={{ cursor: 'pointer' }}>
                  Email {renderSortArrow('email')}
                </th>
                <th
                  onClick={() => handleSort('ready')}
                  style={{ cursor: 'pointer' }}>
                  Ready {renderSortArrow('ready')}
                </th>
                <th
                  onClick={() => handleSort('sent')}
                  style={{ cursor: 'pointer' }}>
                  Sent {renderSortArrow('sent')}
                </th>
                <th
                  onClick={() => handleSort('follow_up')}
                  style={{ cursor: 'pointer' }}>
                  Follow Up {renderSortArrow('follow_up')}
                </th>
              </tr>
            </thead>
            <tbody>
              {currentRows.map((row) => (
                <tr key={row.id}>
                  <td>
                    <input
                      type='checkbox'
                      checked={selectedRows.some(
                        (selectedRow) => selectedRow.id === row.id
                      )}
                      onChange={(event) => handleCheckboxChange(event, row)}
                    />
                  </td>
                  <td>{row.id}</td>
                  <td>{row.client_number}</td>
                  <td>{row.client_name}</td>
                  <td>{row.email}</td>
                  <td>{renderBooleanCell(row.ready)}</td>
                  <td>{renderBooleanCell(row.sent)}</td>
                  <td>{renderBooleanCell(row.follow_up)}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <div style={{ marginTop: '20px' }}>
            <Button
              variant='contained'
              color='primary'
              disabled={page === 0}
              onClick={() => setPage((prevPage) => Math.max(prevPage - 1, 0))}>
              Previous
            </Button>
            <span style={{ margin: '0 10px' }}>Page {page + 1}</span>
            <Button
              variant='contained'
              color='primary'
              disabled={(page + 1) * pageSize >= rows.length}
              onClick={() => setPage((prevPage) => prevPage + 1)}>
              Next
            </Button>
            <Button
              variant='contained'
              color='primary'
              style={{ marginLeft: '20px' }}
              Number
              of
              Clients
              onClick={() => sendEmail('statement')}
              disabled={selectedRows.length === 0}>
              Send Statement Email
            </Button>
            <Button
              variant='contained'
              color='secondary'
              style={{ marginLeft: '20px' }}
              onClick={() => sendEmail('followup')}
              disabled={selectedRows.length === 0}>
              Send Followup Email
            </Button>
          </div>
        </>
      )}
    </div>
  );
};

CustomDataGrid.propTypes = {
  rows: PropTypes.array,
  selectedRows: PropTypes.array,
  error: PropTypes.string,
  loading: PropTypes.bool,
  page: PropTypes.number,
  sortConfig: PropTypes.object,
  refresh: PropTypes.bool,
  searchQuery: PropTypes.string,
};

export default CustomDataGrid;
